let heroe: boolean = true;
let villano: boolean = false;

// operador conjuncion (y)
console.log( 'Es heroe y villano', heroe && villano );

// operador disjuncion (o)
console.log( 'Es heroe o villano', heroe || villano );