const valor1: number = 30;
const valor2: number = 4;

// operadores aritmeticos
console.log('suma => ', valor1 + valor2);
console.log('resta => ', valor1 - valor2);
console.log('multiplicacion => ', valor1 * valor2);
console.log('division => ', valor1 / valor2);
console.log('residuo => ', valor1 % valor2);