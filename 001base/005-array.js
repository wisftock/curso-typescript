var miLista = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
// se crear un array y se le asigna Array y que tipo de variable va a ser
var misDatos = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
// se crear un array y se le asigna de que tipo de variable que va a ser
var misNumeros = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
// se crear un array y se le asigna de que tipo de variable que va a ser
var misLetras = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i'];
console.log(misLetras);
// agregar contendido
misLetras.push('agregado');
console.log(misLetras);
// cantidad de elementos
console.log(misLetras.length);
// recorrer un array
misLetras.forEach(function (elemento) {
    console.log(elemento);
});
// join
console.log(misLetras.join());
console.log(misLetras.join(''));
console.log(misLetras.join(' - '));
