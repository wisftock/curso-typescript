// tupla = array de elementos

let tupla : [number, number];
let tupla2 : [number, number];

tupla = [2,5];
tupla2 = [4,8];

const resultado : [number, number] = [ tupla[0] + tupla2[0], tupla[1] + tupla2[1] ]

console.log(resultado);

const tupla_5_elementos: [number, number, number, number, number] = [1,2,3,4,5];

const profesores: [string, number] = ['pepe', 50];
const alumno: [string, number] = ['kike', 150];

console.log(`El alumno ${alumno[0]} mide ${alumno[1]}`);
