function multiplicar(numero1, numero2) {
    return numero1 * numero2;
}
var numero = multiplicar(2, 3);
console.log(numero);
// asignar los tipos en los parametros , antes de las llaves se le asigna el tipo que va a retornar la funcion
function sumar(x, y) {
    return x + y;
}
// se le asigna el dipo a la constante
var suma = sumar(3, 4);
console.log(suma);
// 
var sumatoria = 0;
console.log(sumatoria, 'antes de ejecutar');
// la funcion no retorna nada entonces se le agrega :void
function valores() {
    sumatoria++;
}
valores();
console.log(sumatoria, 'despues de ejecutart');
// guardar el resultado en una variable
var data = 0;
function datos() {
    return data++;
}
datos();
var result = data;
console.log(result);
