function multiplicar(numero1, numero2){
    return numero1 * numero2;
}
const numero = multiplicar(2,3);
console.log(numero)


// asignar los tipos en los parametros , antes de las llaves se le asigna el tipo que va a retornar la funcion
function sumar(x:number, y:number): number{
    return x + y;
}
// se le asigna el dipo a la constante
const suma: number = sumar(3,4)
console.log(suma);

// 
let sumatoria = 0;
console.log(sumatoria, 'antes de ejecutar');


// la funcion no retorna nada entonces se le agrega :void
function valores():void {
    sumatoria++
}
valores();
console.log(sumatoria, 'despues de ejecutart');

// guardar el resultado en una variable
let data = 0;
function datos():number {
    return ++data
}
datos()
const result = data;
console.log(result);