function datos(name, lastname) {
    return name + " " + lastname;
}
var resultados = datos('pepe', 'carlos');
console.log(resultados);
// parametro opcional
function datoss(name, lastname) {
    if (lastname) {
        return name + " " + lastname;
    }
    else {
        return name;
    }
}
var resultado = datoss('pepe');
console.log(resultado);
// parametros por default
function valor(name, lastname, edad) {
    if (edad === void 0) { edad = 20; }
    return name + " " + lastname + " " + edad;
}
var respuesta = valor('pepe', 'carlos');
console.log(respuesta);
