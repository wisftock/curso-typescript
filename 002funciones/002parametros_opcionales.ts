function datos(name: string, lastname: string): string{
    return `${name} ${lastname}`
}
const resultados: string = datos('pepe', 'carlos');
console.log(resultados)

// parametro opcional
function datoss(name: string, lastname?: string): string{
    if(lastname){
       return `${name} ${lastname}` 
    } else {
        return name
    }
    
}
const resultado: string = datoss('pepe');
console.log(resultado)

// parametros por default
function valor(name: string, lastname: string, edad:number=20): string{
    return `${name} ${lastname} ${edad}`
}
let respuesta: string = valor('pepe', 'carlos');
console.log(respuesta)