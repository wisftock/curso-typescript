// para el uso de res se le asigna los ( ...valo: tipo[] )

function deporte(persona: string, ...deporte: string[]): string{
    console.log(deporte);
    return `A ${persona} le gusta los deportes como: ${deporte}`;
}
deporte('pepe', 'tenis', 'futbol', 'natacion');

console.log(deporte('pepe', 'tennis', 'futbol', 'natacion'));