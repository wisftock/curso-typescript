interface Casa{
    ancho: number;
    largo: number;
    // atributo opcional se le agrega el ' ? '
    alto?: number;
}

function dimensiones(medidas:Casa):string{
    let area: number = medidas.ancho * medidas.largo;
    if(medidas.alto){
        return `El area de la casa es ${area} mt y de alto ${medidas.alto}`
    } else {
        return `El area de la casa es ${area}`
    }
}

console.log(dimensiones({ancho: 14, largo: 34, alto: 10}))
console.log(dimensiones({ancho: 30, largo: 50, }))