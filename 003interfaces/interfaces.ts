interface Perro{
    nombre: String
}

function adoptar(mascota:Perro):void {
    console.log(`Adoptando a ${mascota.nombre}`)
}

let miMascota = {nombre: 'roky'}
adoptar(miMascota)