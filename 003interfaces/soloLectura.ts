// solo lectura; no se puede sobreescribir 
interface Punto{
    readonly coordenadaX: number;
    readonly coordenadaY: number 
}

let punto: Punto= {coordenadaX: 40, coordenadaY: 34}
console.log(punto);
