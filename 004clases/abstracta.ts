abstract class SuperClase {
   abstract metodoSobreEscribir():void;

   saludar():void{
       console.log('Hello');
   }
}

class claseDerivada extends SuperClase{
    constructor(){
        super()
    }
    metodoSobreEscribir():void{
        console.log('codigo nuevo');
        
    }
}

let instancia = new claseDerivada();
instancia.saludar();
instancia.metodoSobreEscribir();