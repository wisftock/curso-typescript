var Vehiculo = /** @class */ (function () {
    // constructor
    function Vehiculo(marca, fecha, color, puertas) {
        this.kilometraje = 0;
        console.log(this, 'Antes de iniciarlizar las variables');
        this.marca = marca;
        this.fecha = fecha;
        this.color = color;
        this.puertas = puertas;
        console.log(this, 'Despues de inicializar las variables');
    }
    // method
    Vehiculo.prototype.avanzar = function () {
        this.kilometraje = this.kilometraje + 100;
    };
    return Vehiculo;
}());
var vehiculos = new Vehiculo('ford', '2020', 'orange', 5);
console.log(vehiculos);
console.log(vehiculos.kilometraje, 'Antes de avanzar');
vehiculos.avanzar();
console.log(vehiculos.kilometraje, 'Despues de avanzar');
