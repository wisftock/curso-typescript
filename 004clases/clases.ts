class Vehiculo{
    // atributos o propiedaddes sin inicializar
    marca: string;
    fecha: string;
    color: string;
    puertas: number;
    kilometraje: number = 0;

    // constructor
    constructor(marca:string, fecha: string, color: string, puertas: number) {
        console.log(this, 'Antes de iniciarlizar las variables')
        this.marca = marca;
        this.fecha = fecha;
        this.color = color;
        this.puertas = puertas;
        console.log(this, 'Despues de inicializar las variables');
    }
    // method
    avanzar(){
        this.kilometraje = this.kilometraje + 100;
    }
}
let vehiculos = new Vehiculo('ford','2020','orange',5);
console.log(vehiculos);

console.log(vehiculos.kilometraje, 'Antes de avanzar');
vehiculos.avanzar();
console.log(vehiculos.kilometraje, 'Despues de avanzar');