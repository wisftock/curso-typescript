let permiso = true;
class Trabajador {
    get nombreCompleto() {
        return this.nombre;
    }
    set agregarNombre(nombre) {
        if (permiso) {
            this.nombre = nombre;
        }
        else {
            console.log('No tiene permisos');
        }
    }
}
let empleado = new Trabajador();
empleado.nombre = 'Pedro';
console.log(empleado.nombreCompleto);
empleado.agregarNombre = 'Pepe';
console.log(empleado.nombreCompleto);
