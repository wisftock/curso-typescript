let permiso:boolean = true;

class Trabajador{
    nombre:string;
    get nombreCompleto():string{
        return this.nombre
    }
    set agregarNombre(nombre:string){
        if(permiso){
            this.nombre = nombre;
        } else {
            console.log('No tiene permisos');
        }
    }
}

let empleado = new Trabajador();
empleado.nombre = 'Pedro'
console.log(empleado.nombreCompleto);

empleado.agregarNombre = 'Pepe';
console.log(empleado.nombreCompleto);
