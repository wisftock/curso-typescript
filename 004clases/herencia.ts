class Animal{
    caminar(distancia:number):void{
        console.log(`Se movio ${distancia} metros`)
    }
}

class Gato extends Animal{
    maullar():void {
        console.log('miauuu xd');
    }
}

let gatito = new Gato();
gatito.maullar();

gatito.caminar(10);