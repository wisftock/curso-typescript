class Animales{
    nombre: string;
    constructor(nombre: string){
        console.log('Constructor de animales');
        this.nombre = nombre;
    }
    caminar(distancia: number= 0):void{
        console.log(`${this.nombre} camino ${distancia} metros`)
    }
}

class Serpiente extends Animales{
    longitud:number;
    constructor(nombre: string, longitud: number){
        console.log('Constructor de serpiente');
        super(nombre) 
        this.longitud = longitud
    }
   caminar(distancia:number =  5){
       console.log('deslizando');
       super.caminar(distancia)
   }
}

class caballo extends Animales {
    constructor(nombre:string){
        super(nombre)
    }
}

let sam = new Serpiente('pithon', 20);
let zeus = new caballo('woody'); 

sam.caminar()
