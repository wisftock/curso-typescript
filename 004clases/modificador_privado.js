var Jugador = /** @class */ (function () {
    function Jugador(posicion) {
        this.posicion = posicion;
    }
    Jugador.prototype.obtener_posicion = function () {
        console.log(this.posicion);
        this.sobreescribir_privatePosition();
        console.log(this.posicion);
    };
    Jugador.prototype.sobreescribir_privatePosition = function () {
        this.posicion = 'Arquero';
    };
    return Jugador;
}());
var ronaldo = new Jugador('Delantero');
ronaldo.obtener_posicion();
