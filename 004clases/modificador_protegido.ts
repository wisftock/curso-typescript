class Instrumento{
    protected nombre: string;
    constructor(nombre:string){
        this.nombre = nombre;
    }   
}

class Piano extends Instrumento{
    private cuerda: boolean = false;
    constructor(nombre:string){
        super(nombre);
    }
    public obtener_nombre():void {
        console.log(this.nombre);
        
    }
}

let miPiano = new Piano('Mi piano');
miPiano.obtener_nombre();