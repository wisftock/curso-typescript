var Unificador = /** @class */ (function () {
    function Unificador() {
    }
    return Unificador;
}());
// T simboliza numero
var mi_unificador = new Unificador();
mi_unificador.valor = 20;
mi_unificador.sumar = function (valor1, valor2) {
    return valor1 + valor2;
};
console.log(mi_unificador.sumar(4, 8));
console.log(mi_unificador.valor);
// T simboliza string
var mi_unificador2 = new Unificador();
mi_unificador2.valor = "20";
mi_unificador2.sumar = function (valor1, valor2) {
    return valor1 + valor2;
};
console.log(mi_unificador2.sumar("4", "8"));
console.log(mi_unificador2.valor);
