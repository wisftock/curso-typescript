class Unificador<T>{
    valor:T ;
    sumar: (valor1: T, valor2: T) => T;
}

// T simboliza numero
let mi_unificador = new Unificador<number>();
mi_unificador.valor= 20;

mi_unificador.sumar = function(valor1, valor2){
    return valor1 + valor2;
}
console.log(mi_unificador.sumar(4,8));

console.log(mi_unificador.valor);

// T simboliza string
let mi_unificador2 = new Unificador<string>();
mi_unificador2.valor= "20";

mi_unificador2.sumar = function(valor1, valor2){
    return valor1 + valor2;
}
console.log(mi_unificador2.sumar("4","8"));

console.log(mi_unificador2.valor);