function elemento_random(lista) {
    var indice_random = Math.floor(Math.random() * lista.length);
    return lista[indice_random];
}
//  T en este ejemplo simboliza un numero
var numeros = [32, 54, 1, 4];
var mi_numero = elemento_random(numeros);
console.log(mi_numero);
// T en este ejemplo simboliza una letra
var letras = ["q", "c", "k", "a"];
var mi_letra = elemento_random(letras);
