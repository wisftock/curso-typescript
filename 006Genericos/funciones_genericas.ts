
function elemento_random<T>(lista: T[]): T{
    let indice_random: number = Math.floor(Math.random()*lista.length);
    return lista[indice_random];
}
//  T en este ejemplo simboliza un numero
let numeros: number[] = [32,54,1,4];
let mi_numero: number = elemento_random(numeros);

console.log(mi_numero);

// T en este ejemplo simboliza una letra
let letras:  string[] = ["q", "c", "k", "a"];
let mi_letra: string = elemento_random(letras);