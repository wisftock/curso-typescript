interface Datos<T = string>{
    nombre: T
}
let persona_libre: Datos = {nombre: "pepe"}
let persona_presa: Datos<number> = {nombre: 234}

console.log(persona_libre);
console.log(persona_presa);

