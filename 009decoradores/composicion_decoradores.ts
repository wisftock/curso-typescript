function entrando (clase: Object){
    console.log('primer decorador');
}

function imprimiendo(clase: Object){
    console.log('segundo decorador');
}

@entrando
@imprimiendo

class Animal{
    raza:string = 'dog'
}

let my_name: Animal = new Animal()