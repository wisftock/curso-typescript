
// crear un decorador es crear una function y utilizar su nombre  como una etiqueta en otra parte
function imprimir(clase: Function){
    clase.prototype.nombre = "juan";
    console.log(clase.prototype);
    
}
@imprimir

class Persona{
    mensaje: string = 'Hello friend'
    saludar(){
        console.log(this.mensaje);
        
    }
}

let yo: Persona = new Persona()
yo.saludar()

let tu: Persona = new Persona()
tu.saludar()