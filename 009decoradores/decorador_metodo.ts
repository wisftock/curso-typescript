class Estudiante{
    notas: number[];

    constructor(notas: number[]){
        this.notas = notas;
    }
    @modificar_metodo(false)
    obtenerNotas(){
        for(let nota of this.notas){
            console.log(nota);
            
        }
        return this.notas
    }
}

function modificar_metodo(valor:boolean){
    return function(objetivo: any, propiedad:string , descriptor: PropertyDescriptor){
        console.log(objetivo, 'objetivo');
        console.log(propiedad, 'propiedad');
        console.log(descriptor, 'descriptor');
        descriptor.value = valor;
        console.log(descriptor, 'descriptor');
    }
}

let estudiante = new Estudiante([12,9,20,18])
estudiante.obtenerNotas
