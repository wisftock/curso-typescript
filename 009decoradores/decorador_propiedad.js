"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var Planeta = /** @class */ (function () {
    function Planeta(nombre) {
        this.nombre = nombre;
    }
    __decorate([
        decorador_propiedad
    ], Planeta.prototype, "nombre", void 0);
    return Planeta;
}());
function decorador_propiedad(objetivo, propiedad) {
    console.log(objetivo, 'objetivo');
    console.log(propiedad, 'propiedad');
    var respaldo = "respaldo";
    var getter = function () {
        console.log(this[respaldo], "valor de propiedad");
        return this[respaldo] + "...";
    };
    var setter = function (valor) {
        this[respaldo] = valor;
    };
    Object.defineProperty(objetivo, propiedad, {
        get: getter,
        set: setter
    });
}
var planeta = new Planeta("venus");
var planeta2 = new Planeta("tierra");
console.log(planeta.nombre);
