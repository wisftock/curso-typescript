class Planeta{
    @decorador_propiedad
    nombre: string;
    constructor(nombre:string){
        this.nombre = nombre
    }
}

function decorador_propiedad(objetivo: Object, propiedad: string){
    console.log(objetivo, 'objetivo');
    console.log(propiedad, 'propiedad');
    
    const respaldo = "respaldo";
    
    const getter = function(this: any){
        console.log(this[respaldo], "valor de propiedad");
        return this[respaldo] + "..."
    }

    const setter = function(this: any, valor: any){
        this[respaldo] = valor;
    }

    Object.defineProperty(objetivo, propiedad,{
        get: getter,
        set: setter
    })
}

let planeta = new Planeta("venus");
let planeta2 = new Planeta("tierra")
console.log(planeta.nombre);
